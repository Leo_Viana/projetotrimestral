unit Unit1;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls, Mask, Vcl.Imaging.jpeg, Vcl.ExtCtrls;

type
  TForm1 = class(TForm)
    EditNome: TEdit;
    MaskEditDataNasc: TMaskEdit;
    EditTurma: TEdit;
    ListBoxEstudantes: TListBox;
    BtnEnviar: TButton;
    BtnApagar: TButton;
    BtnAtt: TButton;
    BtnLimpar: TButton;
    Image1: TImage;
    procedure EditNomeClick(Sender: TObject);
    procedure EditTurmaClick(Sender: TObject);
    procedure BtnEnviarClick(Sender: TObject);
    procedure BtnApagarClick(Sender: TObject);
    procedure BtnLimparClick(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

  Testudantes = class(TObject)
    nome: string;
    datanascimento: string;
    turma: string;
    end;

var
  Form1: TForm1;
  estudantes : Testudantes;

implementation

{$R *.dfm}

procedure TForm1.EditNomeClick(Sender: TObject);
begin
      EditNome.clear;
end;

procedure TForm1.EditTurmaClick(Sender: TObject);
begin
      EditTurma.clear;
end;

procedure TForm1.BtnEnviarClick(Sender: TObject);
begin
     ListBoxEstudantes.Items.add(EditNome.Text);
     ListBoxEstudantes.Items.add(MaskEditDataNasc.Text);
     ListBoxEstudantes.Items.add(EditTurma.Text);
end;

procedure TForm1.BtnLimparClick(Sender: TObject);
begin
        ListBoxEstudantes.Clear;
end;

procedure TForm1.BtnApagarClick(Sender: TObject);
begin
     ListBoxEstudantes.DeleteSelected;
end;

end.
